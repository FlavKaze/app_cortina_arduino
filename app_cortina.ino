#include <SPI.h> 
#include <UIPEthernet.h>

#include <Stepper.h> //INCLUSÃO DE BIBLIOTECA
const int stepsPerRevolution = 65; //NÚMERO DE PASSOS POR VOLTA
Stepper myStepper(stepsPerRevolution, 4,6,5,7); //INICIALIZA O MOTOR UTILIZANDO OS PINOS DIGITAIS 4, 6, 5, 7

const byte NumModules = 1;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; //ATRIBUIÇÃO DE ENDEREÇO MAC AO ENC28J60
byte ip[] = { 192, 168, 0, 175 };
EthernetServer server(80);

String readString = String(50);
int status_1 = 0;

void setup(){
  Ethernet.begin(mac, ip); //PASSA OS PARÂMETROS PARA A FUNÇÃO QUE VAI FAZER A CONEXÃO COM A REDE
  server.begin(); //INICIA O SERVIDOR PARA RECEBER DADOS NA PORTA 80
  Serial.begin(9600);
  myStepper.setSpeed(300); //VELOCIDADE DO MOTOR

}
void loop(){
    EthernetClient client = server.available(); //CRIA UMA CONEXÃO COM O CLIENTE
    if (client) {
      
        while (client.connected()) {
          
            if (client.available()) {
              
                char c = client.read(); //LÊ CARACTER A CARACTER DA REQUISIÇÃO HTTP
                
                if (readString.length() < 100){
                    readString += c;
                }  
                
                if (c == '\n') { //SE ENCONTRAR "\n" É O FINAL DO CABEÇALHO DA REQUISIÇÃO HTTP, FAZ
                  Serial.println(readString);
                    if (readString.indexOf("?") <0){
                    }else{
                        if(readString.indexOf("abre") >0 and status_1 == 0){
                             Serial.println(readString.indexOf("abre"));
                             for(int i = 0; i < 50; i++){ //PARA "i" IGUAL A 0, ENQUANTO "i" MENOR QUE 50 INCREMENTA "i"
                                 myStepper.step(stepsPerRevolution); //GIRA O MOTOR NO SENTIDO ANTI-HORÁRIO
                             }
                             status_1 = 1;
                             
                         }else if(readString.indexOf("fecha") >0 and status_1 == 1){
                             Serial.println(readString.indexOf("fecha"));
                             for(int i = 0; i < 50; i++){//PARA "i" IGUAL A 0, ENQUANTO "i" MENOR QUE 50 INCREMENTA "i"
                                 myStepper.step(-stepsPerRevolution); //GIRA O MOTOR NO SENTIDO HORÁRIO
                             }
                             status_1 = 0;             
                         }

                    }
                    readString=""; //A VARIÁVEL É REINICIALIZADA
                    client.stop(); //FINALIZA A REQUISIÇÃO HTTP E DESCONECTA O CLIENTE
                 }
            }
        }
    }
}
